package vn.camera;

import android.graphics.Bitmap;
import android.graphics.SurfaceTexture;

/**
 * Copyright © 2016 Neo-Lab Co.,Ltd.
 * Created by Hieu➈ on 09/11/2016.
 */

public class CameraCallback {
    public interface DataCallback {
        void onDataCallback(Bitmap pData);
        void onDataCallback(SurfaceTexture pData);
    }
}
