package vn.camera;

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.support.v4.util.Pair;
import android.util.Log;
import android.view.TextureView;

import java.io.IOException;

/**
 * Copyright © 2016 Neo-Lab Co.,Ltd.
 * Created by Hieu➈ on 01/11/2016.
 *
 * Suggest: CameraV1 only use for android version < LOLLIPOP
 * If Android version >= LOLLIPOP will be use CameraV2
 */

@SuppressWarnings("deprecation")
@Deprecated
public class CameraV1 implements ICamera {
    
    private static final String TAG = CameraV1.class.getSimpleName();

    private Context mContext;

    private CameraCallback.DataCallback mDataCallback;

    private AutoFitTextureView mTextureView;

    private Camera mCamera;

//    private Camera.CameraInfo mBackCameraInfo;

    private TextureView.SurfaceTextureListener mSurfaceTextureListener = new TextureView.SurfaceTextureListener() {
        @SuppressWarnings("ConstantConditions")
        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i1) {
            setSurfaceTextureAvailable(surfaceTexture, i, i1);
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i1) {
            setSurfaceTextureSizeChanged(surfaceTexture, i, i1);
        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {

            setSurfaceTextureDestroyed(surfaceTexture);

            return true;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
            setSurfaceTextureUpdated(surfaceTexture);
        }
    };

    @Override
    public void setSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i1) {
        mCamera = Camera.open(getBackCamera().second);
        try {
            mCamera.setPreviewTexture(surfaceTexture);
            mCamera.startPreview();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        if(mCamera != null){
            mCamera.stopPreview();
            mCamera.release();
            mCamera = null;
        }
    }

    @Override
    public void setSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i1) {

    }

    @Override
    public void setSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
        if(mDataCallback != null) mDataCallback.onDataCallback(surfaceTexture);
    }

    @Override
    public void setConfig(Context pContext) {
        mContext = pContext;
    }

    @Override
    public void doTakePhoto() {

    }

    @Override
    public void doRecordVideo() {

    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void setTextureView(AutoFitTextureView pTextureView) {
        mTextureView = pTextureView;
    }

    @Override
    public void open() {
        mTextureView.setSurfaceTextureListener(mSurfaceTextureListener);
    }

    private Pair<Camera.CameraInfo, Integer> getBackCamera() {
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        final int numberOfCameras = Camera.getNumberOfCameras();

        for (int i = 0; i < numberOfCameras; ++i) {
            Camera.getCameraInfo(i, cameraInfo);
            if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                return new Pair<Camera.CameraInfo, Integer>(cameraInfo, i);
            }
        }
        return null;
    }

    @Override
    public void setRotation(int rotation) {

    }

    @Override
    public void setDataCallback(CameraCallback.DataCallback pDataCallback) {
        mDataCallback = pDataCallback;
    }

    @Override
    public void setSurfaceTextureListener(TextureView.SurfaceTextureListener pSurfaceTextureListener) {
        mSurfaceTextureListener = pSurfaceTextureListener;
    }
}
