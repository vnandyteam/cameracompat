package vn.camera;

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.view.TextureView;

/**
 * Copyright © 2016 Neo-Lab Co.,Ltd.
 * Created by Hieu➈ on 01/11/2016.
 */

public interface ICamera {

    void setConfig(Context pContext);

    /**
     * take picture from camera
     * */
    void doTakePhoto();

    /**
     * recode video from camera
     * */
    void doRecordVideo();

    /**
     * on destroy(release) camera
     * */
    void onDestroy();

    /**
     * on resume camera
     * */
    void onResume();

    /**
     * on pause camera
     * */
    void onPause();

    /**
     * @param pTextureView
     * */
    void setTextureView(AutoFitTextureView pTextureView);

    /**
     * Open camera
     * */
    void open();

    /**
     * set Rotation for camera
     * */
    void setRotation(int rotation);

    /**
     * set Data Callback listener
     * */

    void setDataCallback(CameraCallback.DataCallback pDataCallback);

    void setSurfaceTextureListener(TextureView.SurfaceTextureListener pSurfaceTextureListener);

    void setSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i1);
    void setSurfaceTextureDestroyed(SurfaceTexture surfaceTexture);
    void setSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i1);
    void setSurfaceTextureUpdated(SurfaceTexture surfaceTexture);
}
